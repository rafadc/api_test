Rails.application.routes.draw do
  apipie
  devise_for :users
  root to: 'home#index'

  namespace :api do
    namespace :v1, defaults: { format: 'json' } do
      get '/users/me', controller: 'users', action: 'me'
      resources :users, :only => [:show]
      resources :shelves do
        resources :books
      end
      resources :users, :only => [:destroy]
    end
  end
end
