namespace :factory_girl do
  desc "Verify that all FactoryGirl factories are valid"
  task lint: :environment do
    if Rails.env.test?
      FactoryGirl.lint
    else
      raise "You can only lint in test environment. You can do RAILS_ENV=test bundle exec rake factory_girl:lint"
    end
  end
end
