# Heetch test

I'll be building an API for a book management app. We have different shelves where we can keep the books we are reading. Some sort of poor-man's version of Goodreads.

## Starting the app

Just run

```
rails s
```

Since this is a coding test I will use sqlite3 as a DB just to ease testing the app.

You can seed the sample data with

```
rake db:seed
```

This will add some sample data.

## API

### API Documentation

You can find the api documentation live at

```
/apipie
```

You can also generate the api docs in different formats with

```
rake apipie:static
```

### Api design ###

The API is designed using nested resources. A user contains some shelves that contain some books. They don't make sense in other context. This is done this way because the relations between the resources are of pure and absolute ownership.

The first part of the API just includes the /:user_id part and the /users/ has been removed since we are assuming that, for our context, all the operations only make sense in the context of a user. The only exception is the user removal which will keep the /users prefix.

I've decided to keep the version on the URL since it is more common than using the media type for that even though the media type probably is academically speaking more correct.

## Web app

A simple front is provided. You can just create and delete books. It is just an easy way of invoking an example of the API. This is just a quick and dirty toy.

## Development

You can just run

```
rspec
```

To run the full test suite. In development mode guard is also available to autorun tests.

### Linting factories

Linting factories is a nice process. Often we like all our factories to work by default. We can check it using

```
RAILS_ENV=test bundle exec rake factory_girl:lint
```

These kind of checks should eventually make it into the continuous integration.
