admin = User.create(email: "admin@me.com", password: "adminadmin", role: :admin)

super_important_shelf = Shelf.create(name: "Super important info", user: admin)
Book.create(title: "The meaning of life", shelf: super_important_shelf)
Book.create(title: "The other meaning of life", shelf: super_important_shelf)
Book.create(title: "42", shelf: super_important_shelf)

less_important_shelf = Shelf.create(name: "Not so important info", user: admin)
Book.create(title: "Meh", shelf: less_important_shelf)

user = User.create(email: "user@me.com", password: "useruser", role: :user)

adventure = Shelf.create(name: "Adventure", user: user)

Book.create(title: "The treasure island", shelf: adventure)
Book.create(title: "Moby Dick", shelf: adventure)

good_books = Shelf.create(name: "Good books", user: user)
Book.create(title: "How to learn a language in 10.000 days", shelf: good_books)
Book.create(title: "Learn C++ in 21 days", shelf: good_books)

bad_books = Shelf.create(name: "Bad books", user: user)

Book.create(title: "How to learn a language in 10 days", shelf: bad_books)
Book.create(title: "Learn C++ in a lifetime", shelf: good_books)

User.create(email: "guest@me.com", password: "guestguest", role: :guest)
