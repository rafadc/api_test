class AddUserIdToShelf < ActiveRecord::Migration
  def change
    add_reference :shelves, :user, index: true
  end
end
