class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :name
      t.text :description
      t.belongs_to :shelf

      t.timestamps null: false
    end
  end
end
