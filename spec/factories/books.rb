FactoryGirl.define do
  factory :book do
    title Faker::Book.title
    description Faker::Lorem.paragraph
    shelf
  end
end
