FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    password "sample_password"
    password_confirmation { password }
    role :user

    factory :admin do
      role :admin
    end

    factory :guest do
      role :guest
    end

  end
end
