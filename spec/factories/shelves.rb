FactoryGirl.define do
  factory :shelf do
    name Faker::Book.genre
    description Faker::Lorem.paragraph
    user
  end
end
