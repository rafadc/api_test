require 'rails_helper'
require_relative 'controller_helper'

RSpec.describe Api::V1::UsersController, type: :controller do
  include Devise::TestHelpers

  let!(:user) { create(:user) }
  let(:admin) { create(:admin) }


  context "not logged in" do
    it "is possible to retrieve any user info" do
      get :show, id: user.id, format: :json
      user_response = JSON.parse(response.body)
      expect(user_response["user"]["email"]).to eq(user.email)
    end

    it "is not possible to retrieve my info" do
      get :me, format: :json
      expect(response.code).to eq("403")
    end
  end

  context "as a user" do
    before :each do
      basic_auth_sign_in(user)
    end

    it "is not possible to delete a user" do
      delete :destroy, id: user.id, format: :json
      expect(response.code).to eq("403")
    end

    it "retireves current user" do
      get :me, format: :json
      user_response = JSON.parse(response.body)
      expect(user_response["user"]["email"]).to eq(user.email)
    end
  end

  context "as admin" do
    it "is possible to delete a user" do
      basic_auth_sign_in(admin)
      delete :destroy, id: user.id, format: :json
      expect(User.where(id: user.id)).to be_empty
    end
  end

end
