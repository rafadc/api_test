require 'rails_helper'
require_relative 'controller_helper'

RSpec.describe Api::V1::BooksController, type: :controller do
  include Devise::TestHelpers

  let (:user) { create(:user)}
  let (:admin) { create(:admin)}
  let (:non_owner_user) { create(:user)}
  let (:shelf) { create(:shelf, user: user) }
  let (:book) { create(:book, shelf: shelf) }


  context "not logged in" do
    it "shows the information of a book" do
      get :show, {shelf_id: shelf.id, id: book.id}, format: :json
      book_response = JSON.parse(response.body)
      expect(book_response["book"]["title"]).to eq(book.title)
      expect(book_response["book"]["description"]).to eq(book.description)
    end

    it "does not allow to create a book" do
      post :create, {shelf_id: shelf.id, book: book.attributes}, format: :json
      expect(response.code).to eq("401")
    end

    it "does not allow to update a book" do
      put :update, {shelf_id: shelf.id, id: book.id, book: book.attributes}, format: :json
      expect(response.code).to eq("401")
    end

    it "is forbidden to delete a book" do
      delete :destroy, {shelf_id: shelf.id, id: book.id}, format: :json
      expect(response.code).to eq("401")
    end
  end

  context "as a user" do
    before :each do
      basic_auth_sign_in(user)
    end

    it "returns correct status code on creation and the book with an id" do
      post :create, {shelf_id: shelf.id, book: book.attributes}, format: :json
      book_response = JSON.parse(response.body)
      expect(response.code).to eq("201")
      expect(book_response["book"]["id"]).not_to be_nil
      expect(book_response["book"]["title"]).to eq(book.title)
    end

    it "returns error status code on creation failure with some description" do
      basic_auth_sign_in(user)
      nameless_book = build(:book, title: " ", shelf: shelf)

      post :create, {shelf_id: shelf.id, book: nameless_book.attributes}, format: :json
      book_response = JSON.parse(response.body)
      expect(response.code).to eq("422")
      expect(book_response["errors"]).not_to be_nil
    end

    it "updates a book according to the sent parameters" do
      book.title = "New name"
      put :update, {shelf_id: shelf.id, id: book.id, book: book.attributes}, format: :json
      book.reload
      expect(response.code).to eq("200")
      expect(book.title).to eq("New name")
    end

    it "returns error messages if the book update changes are not allowed" do
      book.title = " "
      put :update, {shelf_id: shelf.id, id: book.id, book: book.attributes}, format: :json
      book_response = JSON.parse(response.body)
      expect(response.code).to eq("422")
      expect(book_response["errors"]).not_to be_nil
    end

    it "removes a book if owner" do
      book_id = book.id
      delete :destroy, {shelf_id: shelf.id, id: book.id}, format: :json
      expect(Book.where(id: book_id)).to be_empty
    end
  end

  context "as the non owner" do
    before :each do
      basic_auth_sign_in(non_owner_user)
    end

    it "does not allow to update a book if not the owner" do
      put :update, {shelf_id: shelf.id, id: book.id, book: book.attributes}, format: :json
      expect(response.code).to eq("403")
    end

    it "you can't delete books" do
      delete :destroy, {shelf_id: shelf.id, id: book.id}, format: :json
      expect(response.code).to eq("403")
    end
  end

  context "as admin" do
    it "you can remove any book" do
      basic_auth_sign_in(admin)
      delete :destroy, {shelf_id: shelf.id, id: book.id}, format: :json
      expect(response.code).to eq("200")
    end
  end
end
