require 'rails_helper'
require_relative 'controller_helper'

RSpec.describe Api::V1::ShelvesController, type: :controller do
  include Devise::TestHelpers

  let (:user) { create(:user)}
  let (:admin) { create(:admin)}
  let (:guest) { create(:guest)}
  let (:non_owner_user) { create(:user)}
  let! (:shelf) { create(:shelf, user: user) }

  context "not logged in" do
    it "retrieves the list of shelves" do
      get :index, { }, format: :json
      shelves_response = JSON.parse(response.body)
      expect(shelves_response["shelves"].size).to eq(1)
    end

    it "retrieves a given shelf" do
      get :show, {id: shelf.id}, format: :json
      shelf_response = JSON.parse(response.body)
      expect(shelf_response["shelf"]["name"]).to eq(shelf.name)
      expect(shelf_response["shelf"]["description"]).to eq(shelf.description)
    end

    it "is not allowed to create a shelf" do
      post :create, {shelf: shelf.attributes}, format: :json
      expect(response.code).to eq("401")
    end

    it "is not allowed to update a shelf" do
      put :update, {id: shelf.id, shelf: shelf.attributes}, format: :json
      expect(response.code).to eq("401")
    end

    it "is forbidden to delete shelves" do
      delete :destroy, {id: shelf.id}, format: :json
      expect(response.code).to eq("401")
    end
  end

  context "as a user" do
    before :each do
      basic_auth_sign_in(user)
    end

    it "is not allowed to create a shelf in other user space" do
      shelf.user = non_owner_user
      post :create, {user_id: non_owner_user.id, shelf: shelf.attributes}, format: :json
      expect(response.code).to eq("403")
    end

    it "returns correct status code on creation success and the shelf with an id" do
      post :create, {user_id: user.id, shelf: shelf.attributes}, format: :json
      shelf_response = JSON.parse(response.body)
      expect(response.code).to eq("201")
      expect(shelf_response["shelf"]["id"]).not_to be_nil
      expect(shelf_response["shelf"]["name"]).to eq(shelf.name)
    end

    it "returns error status code on creation failure with some description" do
      basic_auth_sign_in(user)
      nameless_shelf = build(:shelf, user: user, name: " ")

      post :create, {user_id: user.id, shelf: nameless_shelf.attributes}, format: :json
      shelf_response = JSON.parse(response.body)
      expect(response.code).to eq("422")
      expect(shelf_response["errors"]).not_to be_nil
    end

    it "updates the shelf according to the sent parameters" do
      shelf.name = "New name"
      put :update, {user_id: user.id, id: shelf.id, shelf: shelf.attributes}, format: :json
      shelf.reload
      expect(response.code).to eq("200")
      expect(shelf.name).to eq("New name")
    end

    it "returns error messages if the shelf changes are not allowed" do
      shelf.name = " "
      put :update, {user_id: user.id, id: shelf.id, shelf: shelf.attributes}, format: :json
      shelf_response = JSON.parse(response.body)
      expect(response.code).to eq("422")
      expect(shelf_response["errors"]).not_to be_nil
    end

    it "removes a shelf" do
      shelf_id = shelf.id
      delete :destroy, {user_id: user.id, id: shelf.id}, format: :json
      expect(Shelf.where(id: shelf_id)).to be_empty
      expect(Book.where(shelf_id: shelf_id)).to be_empty
    end
  end

  context "as a guest" do
    it "is not allowed to create a shelf" do
      shelf = create(:shelf, user: guest)
      basic_auth_sign_in(guest)
      post :create, {user_id: guest.id, shelf: shelf.attributes}, format: :json
      expect(response.code).to eq("403")
    end
  end

  context "as an admin" do
    before :each do
      basic_auth_sign_in(admin)
    end

    it "is allowed to create shelves for any user" do
      post :create, {user_id: user.id, shelf: shelf.attributes}, format: :json
      expect(response.code).to eq("201")
    end

    it "is allowed to delete any shelf" do
      delete :destroy, {user_id: user.id, id: shelf.id}, format: :json
      expect(response.code).to eq("200")
    end
  end

  context "as the non owner user" do
    before :each do
      basic_auth_sign_in(non_owner_user)
    end

    it "does not allow to update a shelf if not the owner" do
      put :update, {user_id: user.id, id: shelf.id, shelf: shelf.attributes}, format: :json
      expect(response.code).to eq("403")
    end

    it "is forbidden to delete a shelf" do
      delete :destroy, {user_id: user.id, id: shelf.id}, format: :json
      expect(response.code).to eq("403")
    end
  end
end
