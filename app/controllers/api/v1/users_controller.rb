module Api
  module V1
    class UsersController < ApiController
      before_action :check_auth, except: [:show, :me]
      before_action :check_auth_forcing_forbidden, only: [:me]

      load_and_authorize_resource

      resource_description do
        short 'A user in our system.'
      end

      def show
        render json: @user
      end

      api! "Delete a user. Only admin can perform this action."
      param :id, :number, "The id of the user to delete."
      def destroy
        @user.destroy
        render json: @user, status: 200
      end

      api! """Returns the information about the currently logged in user.
This method breaks the HTTP standard returning 403 in case the credentials are wrong so this can be used as a first request from a browser that does not support inhibiting 401 response."""
      def me
        render json: current_user, status: 200
      end
    end
  end
end
