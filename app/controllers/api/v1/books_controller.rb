module Api
  module V1
    class BooksController < ApiController
      before_action :check_auth, except: [:show, :index]
      load_and_authorize_resource

      resource_description do
        short 'The details of a book'
      end

      def_param_group :book do
        param :book, Hash, :required => true, :action_aware => true do
          param :title, String, "The title of the book."
          param :description, String
        end
      end

      api! "Retrieve a single book."
      param :id, :number, "The id of the book to retrieve."
      def show
        render json: @book
      end

      api! "Create a new book."
      param_group :book
      def create
        if @book.save
          render json: @book, status: 201
        else
          render json: {errors: @book.errors}, status: 422
        end
      end

      api! "Update a book."
      param_group :book
      def update
        if @book.update_attributes(book_params)
          render json: @book, status: 200
        else
          render json: {errors: @book.errors}, status: 422
        end
      end

      api! "Delete a book."
      param :id, :number, "The id of the book to delete."
      def destroy
        @book.destroy
        render json: @book, status: 200
      end

      private
      def book_params
        params.require(:book).permit(:title, :description, :shelf_id)
      end
    end
  end
end
