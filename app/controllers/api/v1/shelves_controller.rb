module Api
  module V1
    class ShelvesController < ApiController
      before_action :check_auth, except: [:show, :index]
      load_and_authorize_resource except: [:index]

      resource_description do
        short 'A shelve is used to categorize the different books into related topics.'
      end

      def_param_group :shelf do
        param :shelf, Hash do
          param :name, String, "The name of the shelf. It will give the name for the category of books."
          param :description, String, allow_nil: true
          param :user_id, String
        end
      end

      api! "Retrieve a single shelf."
      param :id, :number, "The id of the shelf to retrieve."
      example '{"shelf":{"id":1,"name":"Super important info","description":null,"books":[{"id":1,"name":"The meaning of life"},{"id":2,"name":"The other meaning of life"},{"id":3,"name":"42"}]}}'
      def show
        render json: @shelf
      end

      api! "List all shelves."
      example '{"shelves":[{"id":1,"name":"Super important info","description":null,"books":[{"id":1,"name":"The meaning of life"},{"id":2,"name":"The other meaning of life"},{"id":3,"name":"42"}]},{"id":2,"name":"Not so important info","description":null,"books":[{"id":4,"name":"Meh"}]}]}'
      def index
        authorize!(:index, Shelf)
        shelves = Shelf.includes(:books)
        render json: shelves
      end

      api! "Create a new shelf."
      param_group :shelf
      def create
        if @shelf.save
          render json: @shelf, status: 201
        else
          render json: {errors: @shelf.errors}, status: 422
        end
      end

      api! "Update a shelf."
      param_group :shelf
      def update
        if @shelf.update_attributes(shelf_params)
          render json: @shelf, status: 200
        else
          render json: {errors: @shelf.errors}, status: 422
        end
      end

      api! "Delete a shelf. This will delete all books under the shelve."
      param :id, :number, "The id of the shelf to delete."
      def destroy
        @shelf.destroy
        render json: @shelf, status: 200
      end

      private
      def shelf_params
        params.require(:shelf).permit(:name, :description, :user_id)
      end
    end
  end
end
