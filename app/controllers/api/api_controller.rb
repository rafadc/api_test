module Api
  class ApiController < ApplicationController
    skip_before_action :verify_authenticity_token
    respond_to :json

    rescue_from CanCan::AccessDenied do |exception|
      render json: {errors: "Unauthorized"}, status: 403
    end

    def check_auth
      authenticate_or_request_with_http_basic do |email,password|
        sign_user(email, password)
      end
    end

    def check_auth_forcing_forbidden
      authenticate_with_http_basic do |email,password|
        sign_user(email, password)
      end
    end

    private
    def sign_user(email, password)
      user = User.find_by(email: email)
      if user && user.valid_password?(password)
        sign_in :user, user
      end
    end

  end
end
