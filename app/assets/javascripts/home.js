(function($, undefined) {
  "use strict"

  $(function(){

    $("#loginButton").click(function() {
      var username = $("#email").val();
      var password = $("#password").val();

      $.ajaxSetup({
        headers: {
          'Authorization': "Basic " + btoa(username + ":" + password)
        }
      });

      var request = $.get({
        url: "/api/v1/users/me",
        dataType: 'json'
      });

      request.fail(function() {
        showNotice("User or password incorrect");
      });

      request.success(function(result) {
        $("#loginForm").hide();
        $("#content").show();
        window.userId = result.user.id;
        showShelves(result.user.id);
      });

    });
  });
})(jQuery);
