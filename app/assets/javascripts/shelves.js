function showShelves(userId) {
  function onClickDeleteBook() {
    deleteBook($(this).attr("data-delete-shelve-id"), $(this).attr("data-delete-book-id"));
    $(this).parent().remove();
  }

  function onClickCreateBook() {
    $("#shelfId").val($(this).attr("data-shelve-id"));
    window.createBookDialog.dialog( "open" );
  }

  function deleteBook(shelveId, bookId) {
    $.ajax({
      method: "DELETE",
      url: "/api/v1/shelves/"+shelveId+"/books/"+bookId,
      dataType: 'json'
    });
  }

  function drawShelve(shelve) {
    var shelveTemplate = $("#shelveTemplate").html();
    var template = Handlebars.compile(shelveTemplate);
    var result = template(shelve);
    $("#contentList").append(result);
  }

  var request = $.get({
    url: "/api/v1/shelves",
    dataType: 'json'
  });

  request.success(function(data){
    $("#content").empty();
    var contentList = $("<div>").attr("id","contentList").addClass("row");
    $("#content").append(contentList);
    data.shelves.forEach(drawShelve);
    $(".delete-book").click(onClickDeleteBook);
    $(".create-book-link").click(onClickCreateBook);
  });

}
