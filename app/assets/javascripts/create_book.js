$(function() {
  window.createBookDialog = $( "#createBook" ).dialog({
    autoOpen: false,
    height: 400,
    width: 600,
    modal: true,
    buttons: {
      "Create a book": addBook,
      Cancel: function() {
	window.createBookDialog.dialog( "close" );
      }
    },
    close: function() {
      $("#title").val("");
      $("#description").val("");
    }
  });

  window.createBookDialog.find( "form" ).on( "submit", function( event ) {
    event.preventDefault();
    addBook();
  });

  function addBook() {
    var shelfId = $("#shelfId").val();
    var request = $.post({
      url: "/api/v1/shelves/"+shelfId+"/books",
      data: {
        book: {
          shelf_id: shelfId,
          title: $("#title").val(),
          description: $("#description").val()
        }
      },
      dataType: 'json'
    });

    request.success(function() {
      $("#title").val("");
      $("#description").val("");
      window.createBookDialog.dialog( "close" );
      showShelves(window.userId);
    });

    request.fail(function(errors) {
      console.log(errors);
      showNotice(errors.errors);
    });
  }
});
