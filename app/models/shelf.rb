class Shelf < ActiveRecord::Base
  validates_presence_of :name, :user

  belongs_to :user
  has_many :books, dependent: :destroy
end
