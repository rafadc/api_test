class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :shelves

  def guest?
    role == "guest"
  end

  def regular_user?
    role == "user"
  end

  def admin?
    role == "admin"
  end

end
