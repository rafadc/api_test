class Ability
  include CanCan::Ability

  def initialize(user)
    can :show, :all
    can :index, [Shelf, Book]

    return unless user

    can :index, User
    can :me, :all

    if user.admin?
      can [:create, :update, :destroy], :all
    elsif user.regular_user?
      can [:create, :update, :destroy], Shelf do |shelf|
        shelf.user == user
      end
      can [:create, :update, :destroy], Book do |book|
        book.shelf.user == user
      end
    end
  end
end
