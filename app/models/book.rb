class Book < ActiveRecord::Base
  belongs_to :shelf

  validates_presence_of :title, :shelf
end
