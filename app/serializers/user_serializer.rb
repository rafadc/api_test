class UserSerializer < ActiveModel::Serializer
  attributes :id, :email

  class ShelfSerializer < ActiveModel::Serializer
    attributes :id, :name
  end
  has_many :shelves
end
