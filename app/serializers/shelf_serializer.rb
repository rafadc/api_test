class ShelfSerializer < ActiveModel::Serializer
  attributes :id, :name, :description

  class BookSerializer < ActiveModel::Serializer
    attributes :id, :title
  end
  has_many :books, serializer: BookSerializer

end
